Spletna trgovina e-Pesmi

6. vaje pri predmetu Osnove informacijskih sistemov (navodila)
Vzpostavitev repozitorija

Na BitBucket je na voljo javni repozitorij https://bitbucket.org/dlavbic/ois-2015-2016-p3.2, ki vsebuje spletno trgovino e-Pesmi predstavljeno na predavanjih. Z uporabo funkcije Fork ustvarite lastno kopijo repozitorija v okviru katere boste opravljali vaje. V razvojnem okolju Cloud9 ustvarite lokalni repozitorij in ga povežite z oddaljenim BitBucket repozitorijem. V okviru vaj popravite in dopolnite obstoječo implementacijo spletne trgovine kot zahtevajo navodila. Med delom smiselno uveljavljajte spremembe v lokalnem in oddaljenem repozitoriju!
Super količinski popusti

Trenutna implementacija spletne trgovine e-Pesmi ne predvideva dodatnih količinskih popustov, ki bi morda privabili nove stranke. Popravite implementacijo tako, da se pri pripravi e-Računa upoštevajo še spodnji popusti. Popusti se seštevajo ter veljajo za vsako posamezno pesem v košarici.

    20% popust pri nakupu pet ali več pesmi poljubnih izvajalcev,
    10% popust pri nakupu treh ali več pesmi istega žanra ali žanrov,
    5% popust pri nakupu več pesmi izvajalcev Iron Maiden ali Body Count,
    1% popust pri računu izdanem v prvi polovici ure (npr. med 13:00 in 13:30).

Spremenljiva davčna stopnja

Trenutna implementacija spletne trgovine e-Pesmi za vse pesmi predvideva davčno stopnjo 22%. Popravite implementacijo tako, da bodo pesmi legendarnih izvajalcev Queen, Led Zepplin in Kiss ter pa vse pesmi žanrov Metal, Heavy Metal in Easy Listening neobdavčene. Za ostale pesmi se predvideva davčna stopnja 9,5%, dočim pa edino za pesmi izvajalca Justin Bieber ohranite davčno stopnjo 22%. Pazite, da pri pripravi e-Računa ločeno izračunate osnove za posamezno davčno stopnjo. Popravite tudi izpis pesmi na spletni strani trgovine tako, da pri izračunu cene pesmi upoštevate tudi davčno stopnjo ter le-to izpišete kot ... @ 0.95 € (9.5%).
TTL pesmi v košarici

Popravite implementacijo spletne trgovine e-Pesmi tako, da ob dodajanju pesmi v košarico v seji uporabnika shranite tudi trenutni čas (za vsako pesem posebej). Predpostavite, da pesmi, ki so v košarici že dalj časa, uporabniku niso več zanimive ter se jih sme odstraniti. Ob izpisu cene pesmi v košarici prikažite tudi time to live (TTL) pesmi kot ... @ 0.95 € (9.5%) # 57 s, ki naj se osveži enkrat na sekundo ter izpiše odebeljeno, ko je čas do odstranitve manjši od pet sekund.